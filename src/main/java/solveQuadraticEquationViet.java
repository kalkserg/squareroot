
public class solveQuadraticEquationViet implements quadraticEquation{
    public int a,b,c;

    public solveQuadraticEquationViet(int b, int c) {
        this.a = 1;
        this.b = b;
        this.c = c;
    }

    public Integer[] solve() {
        Integer root[] = new Integer[2];

        for(int x1 =-100; x1<=100; x1++)
            for(int x2=-100; x2<=100; x2++){
                if(x1+x2 == -b && x1*x2 == c) {root[0] = x1;root[1] = x2; return root;};
            }
        return null;
    }
}
