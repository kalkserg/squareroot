import org.junit.*;

import static org.junit.Assert.*;

public class solveQuadraticEquationVietTest {

    @BeforeClass
    public static void beforeClass() throws Exception {
        System.out.println("==================== Начало тестов ======================");
    }

    @AfterClass
    public static void afrerClass() throws Exception {
        System.out.println("===================== Конец тестов =======================");
    }

    @Test
    public void shouldRootEquals() {
        solveQuadraticEquationViet eq = new solveQuadraticEquationViet(-5,6);
        Integer[] actual = eq.solve();
        Integer[] expected = new Integer[]{2,3};
        assertEquals(expected, actual);
    }
}